class Cachorro:
	def _init_(self, tamanho = 0.0, peso = 0.0, cor = ""):
		self._tamanho = tamanho
		self._peso = peso
		self._cor = cor

@property
def tamanho(self):
	return self._tamanho

@tamanho.setter
def tamanho(self, tamanho):
	self._tamanho = tamanho

@property
def peso(self):
	return self._peso

@peso.setter
def peso(self):
	self._peso = peso

@property
def cor(self):
	return self._cor

@cor.setter
def cor(self):
	self._cor = cor

def latir(self):
	return "woof woof"

def comer(self):
	return "chomp chomp"
