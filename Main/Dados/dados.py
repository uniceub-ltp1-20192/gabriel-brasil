from Entidades.caneta import Caneta
class Dados:

    def __init__(self):
        self._dados = dict()
        self._identificador = 0
    
    def geradorIdentificador(self):
        self._identificador = self._identificador + 1
        return self._identificador

    def buscarPorIdentificador(self,identificador):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(int(identificador))

    def buscarPorAtributo(self,param):
        if (len(self._dados)) == 0:
            print("Dicionario vazio!")
        else:
            for p in self._dados.value():
                if p.tamanho == param:
                    return p
            return None

    def inserirDados(self,entidade):
        self._ultimoIdentificador = self.geradorIdentificador()
        entidade.identificador = self._ultimoIdentificador
        print(entidade)
        self._dados[self._ultimoIdentificador] = entidade

    def deletar(self,entidade):
        print(entidade)
        del self._dados[entidade.identificador]

    def alterar(self,entidade):
        self._dados[self._ultimoIdentificador] = entidade
