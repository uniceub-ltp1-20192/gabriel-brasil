from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.caneta import Caneta

class Menu:
    
    @staticmethod
    def menuPrincipal():
        print("""
            0 - Sair
            1 - Consultar
            2 - Inserir
            3 - Alterar
            4 - Deletar
            """)
        return Validador.validar("[0-4]","""Opcao do menu de estar entre {}""",
        """Opcao {} Valida!""")
    
    @staticmethod
    def menuConsultar():
        print("""
                0 - Voltar
                1 - Consultar por identificador
                2 - Consultar por propriedade
                """)
        return Validador.validar("[0-2]","""Opcao do menu de estar entre {}""",
        """Opcao {} Valida!""")

    @staticmethod
    def iniciarMenu():
        opMenu = ""
        d = Dados()
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":
                        print("Entrou em consultar por identificador")
                        retorno = Menu.menuBuscarPorIdentificador(d)
                        if retorno != None:
                            print(retorno)
                        else:
                            print("Nao encontrado")
                    elif opMenu == "2":
                        print("Entrou em consultar por Atributo")
                        Menu.menuBuscarPorAtributo(d)
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")  
                opMenu = ""  

            elif opMenu == "2":
                print("Entrei em Inserir")
                Menu.menuInserir(d)
                opMenu = ""

            elif opMenu == "3":
                print("Entrei em Alterar")
                opMenu = Menu.menuConsultar()
                if opMenu == "1":
                        print("Entrou em consultar por identificador")
                        retorno = Menu.menuBuscarPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                        else:
                            print("Nao encontrado")
                elif opMenu == "2":
                        print("entrei em consultar por propriedade")
                opMenu = ""

            elif opMenu == "4":
                print("Entrei em Deletar")
                while opMenu != "0":
                    opMenu = Menu.menuConsultar()
                    if opMenu == "1":   
                        retorno = Menu.menuBuscarPorIdentificador(d)
                        Menu.menuDeletar(d,retorno)

                    elif opMenu == "2":
                        Menu.menuBuscarPorAtributo(d)
                    elif opMenu == "0":
                        print("Saindo")
                    else:
                        print("Digite uma opcao valida!")
                opMenu = "" 
            elif opMenu == "0":
                print("Saindo")
            else:
                print("Digite uma opcao valida!")
                opMenu = ""

    @staticmethod
    def menuBuscarPorIdentificador(d):
        retorno = d.buscarPorIdentificador(Validador.validar(r'\d+','',''))
        return retorno

    @staticmethod
    def menuInserir(d):
        caneta = Caneta()
        caneta.cor = input("informe uma cor: ")
        caneta.tipo = input("informe um tipo de caneta: ")
        caneta.tamanho = input("insira o tamanho: ")
        d.inserirDados(caneta)

    @staticmethod
    def menuAlterar(retorno,d):
        print(retorno)
        retorno.cor = Validador.validarValorEntrada(retorno.cor,"informe uma cor: ")
        retorno.tipo = Validador.validarValorEntrada(retorno.tipo,"informe um tipo de caneta: ")
        retorno.tamanho = Validador.validarValorEntrada(retorno.tamanho,"insira o tamanho: ")
        d.alterar(retorno)

    @staticmethod
    def menuBuscarPorAtributo(d):
        retorno = d.buscarPorAtributo(input("Informe um atributo: "))
        print(retorno)

    @staticmethod
    def menuDeletar(d,entidade):
        print(entidade)
        resposta = input("""Deseja deletar a entidade ?
        S - Sim
        N - Nao
        
        """)
        if(resposta == "S" or resposta == "s"):
            d.deletar(entidade)


