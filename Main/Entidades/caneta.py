from Entidades.lapis import Lapis
class Caneta(Lapis):
	def __init__(self, tinta = "preta"):
		super().__init__()
		self._tinta = "preta"

	@property
	def tinta(self):
		return self._tinta

	@tinta.setter
	def tinta(self, tinta):
		self._tinta = "preta"

	def serApagavel(self):
		print("coisas feitas pela caneta nao podem ser apagadas :(")
	def __str__(self):
		return """Dados de Caneta:
		identficador: {}
		Tamanho: {}
		Tipo: {}
		Cor: {}
		Tinta: {}

		""".format(self.identificador,self.tamanho,self.tipo,self.cor,self.tinta)
	