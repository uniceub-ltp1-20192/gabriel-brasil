def addproducts():
    return int(input("adicionar mais produtos: "))

total = 0
total_discount = 0

while addproducts() != 0 :
    pproducts = float(input("Valor: "))
    dproducts = float(input("Desconto (0-1): "))

    # adiciona ao total o valor do produto
    total = total + pproducts

    # adiciona ao total com desconto o valor descontado
    total_discount = total_discount + (pproducts * (1 - dproducts))

print("\n")
print("-----------------------------------------------")
print("Você pagaria: ", total)
print("Vai pagar: ", total_discount)
